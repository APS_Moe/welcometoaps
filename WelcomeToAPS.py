import time

def introduceUser(user):
    '''
    Prints a static message one character at a time on a single console line.

    :param user: The name oof a person.
    :type user: string
    :return: Print message to console.
    ''' 
    message = f'Hi my name is {user} and I work in a big telly placey!'

    for letter in message:
        time.sleep(0.05)
        print (letter, end='', flush=True)

    time. sleep(1)

    for x in range(28):
        time.sleep(0.01)
        print('\b' * x, end='')
        print(' ' * x, end ='', flush=True)

    print('\b' * 27, end='')
    time.sleep(.5)

    for character in 'am a 3rd Year Computing Student.':
        time.sleep(0.05)
        print (character, end='', flush=True)

    time.sleep(1)
    print ('\n')

def introduceDrew(Name):

    message = f'Hi my name is {Name} and I am a second year computing student.'
    print(message)

    time.sleep(1)
    print ('\n')


def main():
    user = 'John'
    introduceUser(user)
    Name = 'Drew'
    introduceDrew(Name)
    
if __name__ == "__main__":
    main() 